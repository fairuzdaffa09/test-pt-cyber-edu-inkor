// users.js
const express = require('express');

const app = express();
app.use(express.json());

// Data user
const userData = {
    'abc@mail.com': { name: 'John', memberNo: '12345' }
};

// endpoint user
app.post('/user', (req, res) => {
    const { tokenId, email } = req.body;

    // Check if user exists in the database
    const user = userData[email];
    if (!user) {
        return res.status(404).json({ error: 'User not found' });
    }

    // If user exists, send user data
    res.json(user);
});

app.listen(3002, () => {
    console.log('User service is running on port 3002');
});
