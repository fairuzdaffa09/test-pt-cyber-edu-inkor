const express = require('express');
const cookieParser = require('cookie-parser');

const app = express();
app.use(express.json());
app.use(cookieParser());

// fungsi untuk generate token
function generateToken() {
    return '99999999999999999999999999999999';
}

// endpoint untuk service auth
app.post('/login', (req, res) => {
    const { email } = req.body;
    
    if (!email) {
        return res.status(400).json({ error: 'Email is required' });
    }

    const token = generateToken();
    const expirySeconds = 600;
    
    // Set cookie dengan token
    res.cookie('token', token, { maxAge: expirySeconds * 1000, httpOnly: true });
    
    res.json({ token, email, expired: expirySeconds });
});

app.listen(3001, () => {
    console.log('Auth service is running on port 3001');
});
