// posts.js
const express = require('express');

const app = express();
app.use(express.json());

// Data posts
const postData = {
    '12345': { memberNo: '12345', amount: 500000.00 }
};

// endpoint post
app.post('/post', (req, res) => {
    const { memberNo } = req.body;

    // Check if memberNo exists in the database
    const post = postData[memberNo];
    if (!post) {
        return res.status(404).json({ error: 'MemberNo not found' });
    }

    // If memberNo exists, send post data
    res.json(post);
});

app.listen(3003, () => {
    console.log('Post service is running on port 3003');
});
