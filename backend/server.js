const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const { buildSchema } = require('graphql');
const axios = require('axios');

const app = express();

// Definisikan skema GraphQL
const schema = buildSchema(`
  type Query {
    auth(email: String!): Auth
    user(tokenId: String!, email: String!): User
    post(memberNo: String!): Post
  }

  type Auth {
    token: String!
    email: String!
    expired: Int!
  }

  type User {
    name: String!
    memberNo: String!
  }

  type Post {
    memberNo: String!
    amount: Float!
  }
`);

// Resolvers untuk menangani permintaan GraphQL
const root = {
  auth: async ({ email }) => {
    try {
      const response = await axios.post('http://localhost:3001/login', { email });
      console.log("authService - email : " + email);
      return response.data;
    } catch (error) {
      throw new Error(error.response.data.error);
    }
  },
  user: async ({ tokenId, email }) => {
    try {
      const response = await axios.post('http://localhost:3002/user', { tokenId, email });
      console.log("userService - tokenId : " + tokenId +  ", email : " + email);
      return response.data;
    } catch (error) {
      throw new Error(error.response.data.error);
    }
  },
  post: async ({ memberNo }) => {
    try {
      const response = await axios.post('http://localhost:3003/post', { memberNo });
      console.log("postService - memberNo: " + memberNo);
      return response.data;
    } catch (error) {
      throw new Error(error.response.data.error);
    }
  }
};

// Unblock Access-Control-Allow-Origin by CORS policy (jika diperlukan)
app.use("/graphql", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization,Accept,Origin,DNT,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range')
  res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,DELETE,PATCH');
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
      next();
  }
});

// Menambahkan middleware GraphQL ke server Express
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
}));

// Menjalankan server pada port 4000
app.listen(4000, () => {
  console.log('GraphQL server is running on port 4000');
});